set(SOURCES
                Appender.cpp
                AppenderAttachable.cpp
                LayoutAppender.cpp
                FileAppender.cpp
                RollingFileAppender.cpp
                OstreamAppender.cpp
                Layout.cpp
                PatternLayout.cpp
                XmlLayout.cpp
                Logger.cpp
                LoggerStream.cpp
                LoggingEvent.cpp
                Level.cpp
                Filter.cpp
                StringUtil.h
                StringUtil.cpp)

add_library(log4tango_objects OBJECT ${SOURCES})

target_include_directories(log4tango_objects PRIVATE
    $<TARGET_PROPERTY:omniORB4::omniORB4,INTERFACE_INCLUDE_DIRECTORIES>
)

if(WIN32)
    target_compile_definitions(log4tango_objects PRIVATE "${windows_defs}")
else()
    target_compile_options(log4tango_objects PRIVATE -fPIC)
endif()
